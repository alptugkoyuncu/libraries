/*
 * MyADS1248.h
 *
 *  Created on: 9 A�u 2020
 *      Author: ISIK
 */

#ifndef MYADS1248_H_
#define MYADS1248_H_
#include<Arduino.h>
#include <SPI.h>
#include "ads12xx.h"
//end of add your includes here


//#define BOARD1_PARAM
#define BOARD2_PARAM

#ifdef BOARD1_PARAM
#define TEMP1_CURRENT1 I1DIR_AIN3 //IDAC1 : AIN3
#define TEMP1_CURRENT2 I2DIR_OFF//IDAC2=YOK
#define TEMP1_POSITIVE_INPUT MUX_SP2_AIN0 //POSITIVE READ AIN0
#define TEMP1_NEGATIVE_INPUT MUX_SN2_AIN1//NEGATIVE READ AIN1

#define TEMP2_CURRENT1 I1DIR_IEXT2 //IDAC1 : IEXEC2
#define TEMP2_CURRENT2 I2DIR_OFF ///IDAC2=YOK
#define TEMP2_POSITIVE_INPUT  MUX_SP2_AIN4 //POSITIVE READ AIN4
#define TEMP2_NEGATIVE_INPUT MUX_SN2_AIN5 //NEGATIVE READ AIN5

#define TEMP3_CURRENT1 I1DIR_IEXT1 //IDAC1 : IEXEC1
#define TEMP3_CURRENT2 I2DIR_OFF//IDAC2=YOK
#define TEMP3_POSITIVE_INPUT MUX_SP2_AIN7 ////POSITIVE READ AIN7
#define TEMP3_NEGATIVE_INPUT MUX_SN2_AIN6////NEGATIVE READ AIN6
#endif

#ifdef BOARD2_PARAM
#define TEMP1_CURRENT1 I1DIR_IEXT2 //IDAC1 : IEXEC2
#define TEMP1_CURRENT2 I2DIR_OFF//IDAC2=YOK
#define TEMP1_POSITIVE_INPUT MUX_SP2_AIN7 //POSITIVE READ AIN7
#define TEMP1_NEGATIVE_INPUT MUX_SN2_AIN6//NEGATIVE READ AIN6

#define TEMP2_CURRENT1 I1DIR_AIN5 //IDAC1 : AIN5
#define TEMP2_CURRENT2 I2DIR_OFF ///IDAC2=YOK
#define TEMP2_POSITIVE_INPUT  MUX_SP2_AIN3 //POSITIVE READ AIN4
#define TEMP2_NEGATIVE_INPUT MUX_SN2_AIN2 //NEGATIVE READ AIN5

#define TEMP3_CURRENT1 I1DIR_AIN4 //IDAC1 : AIN4
#define TEMP3_CURRENT2 I2DIR_OFF//IDAC2=YOK
#define TEMP3_POSITIVE_INPUT MUX_SP2_AIN1 ////POSITIVE READ AIN1
#define TEMP3_NEGATIVE_INPUT MUX_SN2_AIN0////NEGATIVE READ AIN0
#endif

enum ADSBoardSensorType{
	boardTempUpdated,temp1Updated,temp2Updated,temp3Updated
};

class ADSBoardEvent{
public:
	ADSBoardEvent(ADSBoardSensorType type,double val,double min,double max){
		this->type=type;
		this->val=val;
		this->min=min;
		this->max=max;
	}
	double getVal(){
		return val;
	}
	double getMin(){
		return min;
	}
	double getMax(){
		return max;
	}
	ADSBoardSensorType getType(){return type;}
private:
	double val,min,max;
	ADSBoardSensorType type;
};
class ADSBoardListener{
public:
	virtual void valueUpdated(ADSBoardEvent val)=0;
};

//do reading ile sira ile sicakliklari olcup ortalamasini alir ve listenerlari
// gerektiginde update eder
//getBoardTemp fonksiyonu cagirilirsa isBoardTempRequested true olur
//okunmaktaki olan temp reading bittikten sonra temp okunur ve gonderilir temp reading bolunmez.
//temp reading belirlenen miktarda temp okunur hatali bilgi ayiklanip geri kalanlarin ortalamasi alinir ve listener fire edilir.

#define MINIMUM_WAIT_SECOND_TO_NEW_READ 1000
#define PGA PGA2_4 // PGA 4V/V
#define SPS DOR3_5  //20
#define AKIM IMAG2_100 //i 100 ua
class MyADS1248 {
public:
	MyADS1248(int8_t resetPin=-1);
	void begin(int CS,int start,int DRDY,int res);
	void setListener(ADSBoardListener *l);
	void doReading();
	void setNumberOfPTReading(uint8_t val);
	void requestBoardTemp();
	void fireListener(ADSBoardSensorType type,double avr,double min,double max){
		ADSBoardEvent evt(type,avr,min,max);
		this->fireListener(evt);
	}
	void stop();

	double boardTemp,temp1,temp2,temp3;
private:
	unsigned long lastRead;
	ads12xx ADS;
	bool isBoardTempRequested;
	ADSBoardListener *l;
	ADSBoardSensorType currentReadingSensor;
	void fireListener(ADSBoardEvent evt);
	uint8_t numberOfPtReading;
	const uint8_t numberOfTotalBoardTempReading =5;
	const static uint8_t MAXnumberOfPtReading=20;
	double ptValued[MAXnumberOfPtReading];
	uint8_t readingIndex=0;
	void changeRegistersForNewReading(ADSBoardSensorType type);
	double getPtTemp();
	double getBoardTemp();
	int8_t resetPin;
};
double getTempByMap(double);
#endif /* MYADS1248_H_ */
