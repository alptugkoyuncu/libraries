/*
 * GeneralSystemConstants.h
 *
 *  Created on: 12 Ara 2020
 *      Author: ISIK
 */

#ifndef GENERALSYSTEMCONSTANTS_H_
#define GENERALSYSTEMCONSTANTS_H_

#ifndef SERVER_ID
#define SERVER_ID 1000
#endif
#ifndef NETWORK_PACKAGE_NO_DATA
#define NETWORK_PACKAGE_NO_DATA ""
#endif
#ifndef ESP_ID
#define ESP_ID 999
#endif
#ifndef PING_SENT_INTERVAL
#define PING_SENT_INTERVAL 5000L
#endif
#ifndef PING_WAIT_MS
#define PING_WAIT_MS  60000L
#endif
#ifndef RESET_WAIT_MS
#define RESET_WAIT_MS 180000L
#endif
#endif /* GENERALSYSTEMCONSTANTS_H_ */
