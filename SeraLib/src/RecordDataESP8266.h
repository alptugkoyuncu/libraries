/*
 * RecordData.h
 *
 *  Created on: 25 Mar 2020
 *      Author: alptu
 */

#ifndef RECORDDATA_H_
#define RECORDDATA_H_
#include<Arduino.h>
#include<EEPROM.h>

#define EEPROM_SIZE 512
#define EEPROM_CONNECTION_RECORD_ADDRESS 0
#define LEN_SSID 33
#define LEN_PASS 17
#define LEN_IP 17

class ConnectionRecord {

private:
	uint8_t availableRecord; //99 record var digerleri yok
	char SSID[LEN_SSID];
	char password[LEN_PASS];
	char ip[LEN_IP];
	uint16_t port;
	uint16_t id;
public:
	void setSSID(const char *t){
		memset(SSID,0,LEN_SSID);
		sprintf(SSID,"%s",t);
	}
	void setPassword(const char *t){
		memset(password,0,LEN_PASS);
		sprintf(password,"%s",t);
	}
	void setIp(const char *p){
		memset(ip,0,LEN_IP);
		sprintf(ip,"%s",p);
	}
	void setPort(uint16_t p){
		this->port=p;
	}
	void setId(uint16_t id){
		this->id=id;
	}
	const char *getSSID(){
		return SSID;
	}
	const char *getPassword(){
		return password;
	}
	const char *getIp(){
		return ip;
	}
	const uint16_t getPort(){
		return port;
	}
	const uint16_t getId(){
		return id;
	}


	bool hasAvailableRecord() {
		return this->availableRecord == 99;
	}
	void setAvailableRecord(bool flag){
		this->availableRecord=flag?99:0;
	}
	void resetRecord(){
		this->setAvailableRecord(false);
		memset(this->SSID,0,LEN_SSID);
		memset(this->password,0,LEN_PASS);
		memset(this->ip,0,LEN_IP);
		this->port=0;
		this->id=0;
		EEPROM.begin(EEPROM_SIZE);
		EEPROM.put(EEPROM_CONNECTION_RECORD_ADDRESS, *this);
		EEPROM.commit();
		EEPROM.end();
	}
	void saveConnectionRecord() {
		this->setAvailableRecord(true);
		EEPROM.begin(EEPROM_SIZE);
		EEPROM.put(EEPROM_CONNECTION_RECORD_ADDRESS, *this);
		EEPROM.commit();
		EEPROM.end();
	}
	void loadConnectionRecord(){
		EEPROM.begin(EEPROM_SIZE);
		EEPROM.get(EEPROM_CONNECTION_RECORD_ADDRESS,*this);
		EEPROM.end();
		//kayitli data yoksa default degerler girilir
		if(!this->hasAvailableRecord()){
			this->setAvailableRecord(true);
			this->setSSID("KOYUNCU_CATI");
			this->setPassword("5458798185");
			this->setIp("192.168.1.11");
			this->setPort(5555);
			this->setId(1001);
		}

	}
	//return 0: read and write OK
	//return 1: read ok write error
	//return 2: read error
	uint8_t testEEPROM(){

	}
	int16_t toString(char *buffer,int buffSize){
		if(buffSize<LEN_SSID+LEN_PASS+LEN_IP+4+4)
			return -1;
		return snprintf(buffer,buffSize,"Has Rec:%d\nSSID:%s\nPass:%s\nIp:%s\nPort:%d\nId:%d",this->hasAvailableRecord(),this->getSSID(),this->getPassword(),this->getIp(),this->getPort(),this->getId());
	}
	bool equals(ConnectionRecord rec2){
		if(strcmp(this->getSSID(),rec2.getSSID())!=0)
			return false;
		if(strcmp(this->getPassword(),rec2.getPassword())!=0)
			return false;
		if(strcmp(this->getIp(),rec2.getIp())!=0)
			return false;
		if(this->getId()!=rec2.getId())
			return false;
		if(this->getPort()!=rec2.getPort())
			return false;
		return true;
	}
};


#endif /* RECORDDATA_H_ */
