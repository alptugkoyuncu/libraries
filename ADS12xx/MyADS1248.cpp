/*
 * MyADS1248.cpp
 *
 *  Created on: 9 A�u 2020
 *      Author: ISIK
 */


#include "MyADS1248.h"

double values[20] { 921.6, 960.86, 980.44, 984.36, 988.27, 992.18, 996.09,
		1000.0, 1003.9, 1007.8, 1011.7, 1015.6, 1019.5, 1027.3, 1039.0, 1058.5,
		1077.9, 1116.7, 1194.0, 1460.7 };
int8_t temps[20] { -20, -10, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 7, 10, 15,
		20, 30, 50, 120 };

double getTempByMap(double res) {
	uint8_t aralik;
	for (aralik = 0; aralik < 20; aralik++) {
		if (res == values[aralik])
			return (double) temps[aralik];
		if (res < values[aralik])
			break;
	}
	if (aralik == 0)
		return -20.1;
	double t_ust = (double) temps[aralik];
	double t_alt = (double) temps[aralik - 1];
	double r_ust = values[aralik];
	double r_alt = values[aralik - 1];
	double bolme = (res - r_alt) / (r_ust - r_alt);
	return t_alt + (t_ust - t_alt) * bolme;
}

MyADS1248::MyADS1248(int8_t resetPin) {
	this->l = 0x0;
	this->isBoardTempRequested = false;
	this->currentReadingSensor = ADSBoardSensorType::temp1Updated;
	temp1 = 0.0;
	temp2 = 0.0;
	temp3 = 0.0;
	boardTemp = 0.0;
	this->lastRead = millis();
	this->readingIndex=0;
	this->setNumberOfPTReading(10);
	this->resetPin = resetPin;
	if (this->resetPin >= 0) {
		pinMode(resetPin, OUTPUT);
	}
}

void MyADS1248::begin(int CS, int start, int DRDY,int res) {
	this->resetPin = resetPin;
	if (resetPin >= 0) {
		digitalWrite(resetPin, HIGH);
	}
	ADS.begin(CS, start, DRDY);  //initialize ADS as object of the ads12xx class
	ADS.Reset();
}

void MyADS1248::setListener(ADSBoardListener* l) {
	this->l = l;
}

void MyADS1248::doReading() {
	if ((this->readingIndex
			== 0)&&( millis()-this->lastRead<MINIMUM_WAIT_SECOND_TO_NEW_READ))
		return;
	double min, max;
	if (this->readingIndex == 0 && this->isBoardTempRequested) {
		this->changeRegistersForNewReading(ADSBoardSensorType::boardTempUpdated);
		this->boardTemp = this->getBoardTemp();
		min = this->boardTemp;
		max = this->boardTemp;
		this->isBoardTempRequested = false;
		ADSBoardEvent evt(ADSBoardSensorType::boardTempUpdated, this->boardTemp, min,
				max);
		this->fireListener(evt);
		return;
	}
	if (this->readingIndex == 0) {
		this->changeRegistersForNewReading(this->currentReadingSensor);
	}
	this->ptValued[this->readingIndex++] = this->getPtTemp();

//	Serial.print("T ");
//	Serial.print(this->currentReadingSensor);
//	Serial.print(" : ");
//	Serial.println(this->ptValued[this->readingIndex - 1]);
	if (this->readingIndex >= this->numberOfPtReading) {
		this->lastRead = millis();
		double val = 0;
		min = this->ptValued[0];
		max = this->ptValued[0];
		for (int i = 0; i < this->numberOfPtReading; i++) {
			if (min > this->ptValued[i])
				min = this->ptValued[i];
			if (max < this->ptValued[i])
				max = this->ptValued[i];
			val += this->ptValued[i];
		}
		val = val / (double) this->numberOfPtReading;
		this->readingIndex = 0;
		ADSBoardSensorType typ = this->currentReadingSensor;
		switch (typ) {
		case ADSBoardSensorType::temp1Updated: {
			this->temp1 = val;
			this->currentReadingSensor = ADSBoardSensorType::temp2Updated;
			break;
		}
		case ADSBoardSensorType::temp2Updated: {
			this->temp2 = val;
			this->currentReadingSensor = ADSBoardSensorType::temp3Updated;
			break;
		}
		case ADSBoardSensorType::temp3Updated: {
			this->temp3 = val;
			this->currentReadingSensor = ADSBoardSensorType::temp1Updated;
			break;
		}
		default:
			this->currentReadingSensor = ADSBoardSensorType::temp1Updated;
			break;
		}
		ADSBoardEvent evt(typ, val, min, max);
		this->fireListener(evt);
	}
}

void MyADS1248::requestBoardTemp() {
	this->isBoardTempRequested = true;
}

void MyADS1248::fireListener(ADSBoardEvent evt) {
	if (this->l != 0x0)
		l->valueUpdated(evt);
}

void MyADS1248::changeRegistersForNewReading(ADSBoardSensorType type) {

	if (type == ADSBoardSensorType::boardTempUpdated) {
		ADS.SetRegisterValue(MUX1, MUXCAL2_TEMP | VREFCON1_ON | REFSELT1_ON);
	} else {

		//ADS Reference on REF0, Internal Reference on
		uint8_t readers;
		uint8_t exec;
		switch (type) {
		case ADSBoardSensorType::temp2Updated: {
			readers = TEMP2_POSITIVE_INPUT | TEMP2_NEGATIVE_INPUT;
			exec = TEMP2_CURRENT1 | TEMP2_CURRENT2;
			break;
		}
		case ADSBoardSensorType::temp3Updated: {
			readers = TEMP3_POSITIVE_INPUT | TEMP3_NEGATIVE_INPUT;
			exec = TEMP3_CURRENT1 | TEMP3_CURRENT2;
			break;
		}
		default: {
			readers = TEMP1_POSITIVE_INPUT | TEMP1_NEGATIVE_INPUT;
			exec = TEMP1_CURRENT1 |TEMP1_CURRENT2;
			break;
		}
		}
		ADS.SetRegisterValue(MUX0, readers);
		ADS.SetRegisterValue(VBIAS, VBIAS_RESET);
		//ADS Reference on REF0, Internal Reference on
		ADS.SetRegisterValue(MUX1, REFSELT1_REF0 | VREFCON1_ON);

		//PGA:4 SPS:5
		ADS.SetRegisterValue(SYS0, PGA | SPS);

		//IDAC : 100ma
		ADS.SetRegisterValue(IDAC0, AKIM);		//	IDAC at 0,100mA current

		// IDAC1 : AIN0 IDAC2:YOK
		ADS.SetRegisterValue(IDAC1, exec);		// IDAC Currentsink on AIN1
		ADS.SetRegisterValue(GPIOCFG, 0);
		ADS.SetRegisterValue(GPIODIR, 0);
		ADS.SetRegisterValue(GPIODAT, 0);
	}
	delay(10);
}

double MyADS1248::getPtTemp() {
	unsigned long volt_val = ADS.GetConversion();
	uint8_t pga;
	switch (PGA) {
	case PGA2_0:
		pga = 1;
		break;
	case PGA2_2:
		pga = 2;
		break;
	case PGA2_4:
		pga = 4;
		break;
	case PGA2_8:
		pga = 8;
		break;
	case PGA2_16:
		pga = 16;
		break;
	case PGA2_32:
		pga = 32;
		break;
	case PGA2_64:
		pga = 64;
		break;
	case PGA2_128:
		pga = 128;
		break;
	default:
		pga = 1;
		break;
	}
#define RREF 8250
	double ohm = ((volt_val / 8388608.0) * RREF) / pga;
	return getTempByMap(ohm);
}

void MyADS1248::setNumberOfPTReading(uint8_t val) {
	if (val == 0)
		this->numberOfPtReading = 1;
	else if (val > MAXnumberOfPtReading)
		this->numberOfPtReading = MAXnumberOfPtReading;
	else
		this->numberOfPtReading = val;
}

void MyADS1248::stop() {
	if (this->resetPin >= 0)
		digitalWrite(resetPin, LOW);
}

double MyADS1248::getBoardTemp() {
	unsigned long temp_val = ADS.GetConversion();
	double temp_voltage = (2.048 / 8388608) * temp_val;	//only 23bit data here.
	double temp = ((temp_voltage - 0.118) / (405 * 0.000001)) + 25;	//see Datasheet S.30
	return temp;
}

