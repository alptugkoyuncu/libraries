/*
 * Pt1000ValueMap.h
 *
 *  Created on: 5 Haz 2017
 *      Author: alptug
 */

#ifndef PT1000VALUEMAP_H_
#define PT1000VALUEMAP_H_



double values[20]{
		921.6,
		960.86,
		980.44,
		984.36,
		988.27,
		992.18,
		996.09,
		1000.0,
		1003.9,
		1007.8,
		1011.7,
		1015.6,
		1019.5,
		1027.3,
		1039.0,
		1058.5,
		1077.9,
		1116.7,
		1194.0,
		1460.7
};
int8_t temps[20]{
		-20,
		-10,
		-5,
		-4,
		-3,
		-2,
		-1,
		0,
		1,
		2,
		3,
		4,
		5,
		7,
		10,
		15,
		20,
		30,
		50,
		120
};

double getTempByMap(double res){
	uint8_t aralik;
	for(aralik=0; aralik<20;aralik++){
		if(res==values[aralik])
			return (double)temps[aralik];
		if(res<values[aralik])
			break;
	}
	if(aralik==0)
		return -20.1;
	double t_ust = (double)temps[aralik];
	double t_alt = (double)temps[aralik-1];
	double r_ust = values[aralik];
	double r_alt = values[aralik-1];
	double bolme = (res-r_alt)/(r_ust-r_alt);
	return t_alt+(t_ust-t_alt)*bolme;
}



#endif /* PT1000VALUEMAP_H_ */
