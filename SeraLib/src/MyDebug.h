/*
 * MY_DEBUG.h
 *
 *  Created on: 6 Mar 2020
 *      Author: alptu
 */

#ifndef MY_DEBUG_H_
#define MY_DEBUG_H_

#define AK_DEBUG 0

#include<Arduino.h>


#define d_print(STR)\
  {\
    if(AK_DEBUG){Serial.print("<998:200:");Serial.print(STR);Serial.print(">");} \
  }
#define d_println(STR)\
  {\
    if(AK_DEBUG){Serial.print("<9998:200:"); Serial.print(STR);Serial.println(">");}\
  }
#define d_2print(STR,STR2)\
  {\
    if(AK_DEBUG){Serial.print("<998:200:");Serial.print(STR);Serial.print(STR2);Serial.print(">");} \
  }
#define d_2println(STR,STR2)\
  {\
    if(AK_DEBUG){Serial.print("<998:200:");Serial.print(STR);Serial.print(STR2);Serial.println(">");} \
  }

#endif /* MY_DEBUG_H_ */
