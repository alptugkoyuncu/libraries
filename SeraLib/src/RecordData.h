/*
 * RecordData.h
 *
 *  Created on: 25 Mar 2020
 *      Author: alptu
 */

#ifndef RECORDDATA_H_
#define RECORDDATA_H_
#include<Arduino.h>
#include<EEPROM.h>
#include<MyDebug.h>
#define EEPROM_CONNECTION_RECORD_ADDRESS 100
#define LEN_SSID 33
#define LEN_PASS 17
#define LEN_IP 17
#define LEN_PORT 6
#define LEN_ID 5
struct ConnectionParams {
	char SSID[LEN_SSID];
	char password[LEN_PASS];
	char ip[LEN_IP];
	char port[LEN_PORT];
	char id[LEN_ID];
};
class EEPROMRecord {
	ConnectionParams *params;
private:
	uint8_t availableRecord; //99 record var digerleri yok

public:
	EEPROMRecord(ConnectionParams *p) {
		this->params = p;
	}
	bool hasAvailableRecord() {
		return this->availableRecord == 99;
	}
	void setAvailableRecord(bool flag) {
		this->availableRecord = flag ? 99 : 0;
	}
	void resetRecord() {
		this->setAvailableRecord(false);
		memset(this->params->SSID, 0, LEN_SSID);
		memset(this->params->password, 0, LEN_PASS);
		memset(this->params->ip, 0, LEN_IP);
		memset(this->params->port, 0, LEN_PORT);
		memset(this->params->id, 0, LEN_ID);
		int idx = EEPROM_CONNECTION_RECORD_ADDRESS;
		EEPROM.write(idx, this->availableRecord);
		delay(10);
	}
	bool saveConnectionRecord() {
		int adrr = EEPROM_CONNECTION_RECORD_ADDRESS;
		this->setAvailableRecord(true);
		d_2println(F("params"), params->SSID);
		int idx = EEPROM_CONNECTION_RECORD_ADDRESS;
		char buffer[50];
		bool flag=true;
		EEPROM.write(idx, this->availableRecord);
		idx++;
		EEPROM.put(idx, this->params->SSID);
		delay(100);
		EEPROM.get(idx, buffer);
		delay(100);
		if (strcmp(this->params->SSID, buffer) != 0) {
			d_2println(this->params->SSID, buffer);
			flag = false;
		}
		idx += LEN_SSID;
		EEPROM.put(idx, this->params->password);
		delay(100);
		EEPROM.get(idx, buffer);
		delay(100);
		if (strcmp(this->params->password, buffer) != 0) {
			d_2println(this->params->password, buffer);
			flag = false;
		}

		idx += LEN_PASS;
		EEPROM.put(idx, this->params->ip);
		delay(100);
		EEPROM.get(idx, buffer);
		delay(100);
		if (strcmp(this->params->ip, buffer) != 0) {
			d_2println(this->params->ip, buffer);
			flag = false;
		}

		idx += LEN_IP;
		EEPROM.put(idx, this->params->port);
		delay(25);
		EEPROM.get(idx, buffer);
		delay(25);
		if (strcmp(this->params->port, buffer) != 0) {
			d_2println(this->params->port, buffer);
			flag = false;
		}

		idx += LEN_PORT;
		EEPROM.put(idx, this->params->id);
		delay(25);
		EEPROM.get(idx, buffer);
		delay(25);
		if (strcmp(this->params->id, buffer) != 0) {
			d_2println(this->params->id, buffer);
			flag = false;
		}
		return flag;

	}
	void loadConnectionRecord() {
		int idx = EEPROM_CONNECTION_RECORD_ADDRESS;
		this->availableRecord = EEPROM.read(idx);
		idx++;

		//kayitli data yoksa default degerler girilir
		if (!this->hasAvailableRecord()) {
			strcpy(this->params->SSID, "KOYUNCU_CATI");
			strcpy(this->params->password, "5458798185");
			strcpy(this->params->ip, "192.168.1.7");
			strcpy(this->params->port, "5555");
			strcpy(this->params->id, "1001");
			this->saveConnectionRecord();
		} else {
			EEPROM.get(idx, params->SSID);
			delay(100);
			idx += LEN_SSID;
			EEPROM.get(idx, params->password);
			delay(100);
			idx += LEN_PASS;
			EEPROM.get(idx, params->ip);
			delay(100);
			idx += LEN_IP;
			EEPROM.get(idx, params->port);
			delay(25);
			idx += LEN_PORT;
			EEPROM.get(idx, params->id);
			delay(25);
		}

	}

};

#endif /* RECORDDATA_H_ */
