/*
 * NetworkPackage.cpp
 *
 *  Created on: 12 Ara 2020
 *      Author: ISIK
 */

#include "NetworkPackage.h"


const char *NetworkPackage::SERVER_END_OF_LINE="\r\n";
/*
 unsigned short getUSI(char *val,int len){
 unsigned short value=0;
 int carpan=1;
 for(int i=len-1; i>0; i--){
 if(val[i]>'9' || val[i]<'0')
 return 0;
 value=value+carpan*(short(val[i]-'0'));
 carpan=carpan*10;
 }
 }
 */
NetworkPackage::NetworkPackage() {
	// TODO Auto-generated constructor stub
	this->completed = false;
	this->started = false;
	this->isServerPackage = false;
	this->messageSize = 0;
}

bool NetworkPackage::appendMessage(char c) {
	if (completed)
		return false;
	if (messageSize >= MAXIMUM_PACKAGE_SIZE - 2){
		reset();
		return false;
	}
	if (!started && c != BASLANGIC_AYIRACI)
		return false;
	//if(!(c==BASLANGIC_AYIRACI || c==BITIS_AYIRACI || c=='.'|| c==ANA_DATA_AYIRACI ||c==ARA_DATA_AYIRACI ||(c>='0'&&c<='9')))
//		return false;
		this->message[messageSize++] = c;
	if (c == BASLANGIC_AYIRACI)
		started = true;
	else if (c == BITIS_AYIRACI) {
		completed = true;
		this->message[messageSize] = 0x0;
	}
	return true;
}

bool NetworkPackage::setMessage(char *c, int len) {
	for (int i = 0; i < len; i++)
		message[i] = c[i];
	for (int i = len; i < MAXIMUM_PACKAGE_SIZE; i++)
		message[i] = 0x0;
	this->messageSize = len;
	return true;
}
bool NetworkPackage::isDataCompleted() {
	return completed;
}

char* NetworkPackage::getMessage() {
	return message;
}

uint8_t NetworkPackage::getMessageLength() {
	return messageSize;
}

uint8_t NetworkPackage::getDataOf(int kacinciData, char *ch) {
	if (getDataMiktari() < kacinciData) {

		ch = 0x0;
		return 0;
	}
	int bas = 0, son = 0;
	int flag = 0;
	//dataya kadar olan ana datalari atla
	while (flag < 2) {
		if (message[bas] == ANA_DATA_AYIRACI)
			flag++;
		bas++;
	}
	//ana data ayiracindan sonra bitis ayiraci var data yok demektir
	if (message[bas] == BITIS_AYIRACI) {
		ch = 0x0;
		return 0;
	}
	flag = 0;
	// data icinde arama yap
	// diger  ara datalari atla

	while (flag < kacinciData - 1) {
		if (message[bas] == ARA_DATA_AYIRACI)
			flag++;
		bas++;
	}
	son = bas;

	while (son != messageSize && message[son] != ARA_DATA_AYIRACI
			&& message[son] != BITIS_AYIRACI) {
		son++;
	}
	strncpy(ch, message + bas, son - bas);
	ch[son - bas] = 0x0;
	return son - bas;

}

void NetworkPackage::parse() {
}

void NetworkPackage::reset() {
	this->completed = false;
	this->isServerPackage = false;
	this->messageSize = 0;
	this->started = false;
}

uint8_t NetworkPackage::getDataLine(char *ch) {
	int bas = 0, son = 0;
	//ikinci ana ayiractan sonra data var ikinci ayiraci bul
	int flag = 0;
	for (int i = 0; i < messageSize; i++) {
		if (message[i] == ANA_DATA_AYIRACI)
			flag++;
		if (flag == 2) {
			bas = i + 1;
			break;
		}
	}
	if (message[bas + 1] == BITIS_AYIRACI) {
		ch = 0x0;
		return 0;
	}
	son = messageSize - 1;
	strncpy(ch, message + bas, son - bas);
	ch[son - bas] = 0x0;
	// bitis ayiraciyla data biter bitir bitis ayiracindan 1 eksigi son datadir
	return son - bas;
}
uint16_t NetworkPackage::getActionCode() {
	// action code 1. ana data ayiracindan baslayip 2. ana data ayiracina kadar
	int bas = 0;
	char val[6];
	for (; message[bas] != ANA_DATA_AYIRACI; bas++)
		;
	bas++;
	int son = bas;
	while (message[son] != ANA_DATA_AYIRACI) {
		val[son - bas] = message[son];
		son++;
	}
	val[son - bas] = 0x0;
	return atoi(val);
//	return getUSI(val, son-bas);
}
uint16_t NetworkPackage::getId() {
	unsigned short id;
	//baslangic ayiracini atla
	int bas = 1;
	char val[6];
	int son = bas;
	//id yi ayir ilk ana data ayiracina kadar ki kisim
	while (message[son] != ANA_DATA_AYIRACI) {
		val[son - bas] = message[son];
		son++;
	}
	val[son - bas] = 0x0;
	return atoi(val);
//	return getUSI(val,son-bas);
}

NetworkPackage::~NetworkPackage() {
	// TODO Auto-generated destructor stub
}

uint8_t NetworkPackage::getDataMiktari() {
	int bas = 0, son = 0;
	int flag = 0;
	while (bas < messageSize) {
		if (message[bas] == ANA_DATA_AYIRACI)
			flag++;
		if (flag == 2)
			break;
		bas++;

	}
	bas++;
	// ana data ayiracindan sonra hemen bitis data ayiraci geliyorsa data yoktur
	if (message[bas] == BITIS_AYIRACI)
		return 0;
	flag = 0;
	while (bas < messageSize) {
		if (message[bas] == ARA_DATA_AYIRACI)
			flag++;
		bas++;
	}
	return flag + 1;
}

NetworkPackage NetworkPackage::createServerPackage(uint16_t targetId,
		uint16_t actionCode, char *datas[], uint8_t dataMiktari) {
	NetworkPackage pck;
	pck.changeMessage(targetId, actionCode, datas, dataMiktari);
	return pck;
}
void NetworkPackage::changeMessage(uint16_t targetId, uint16_t actionCode,
		char *datas[], uint8_t dataMiktari) {
//	NetworkPackage pck;
	//	char buffer[50];
	//	int bufferSize = 0;
	//	pck.completed=true;
	//	pck.isServerPackage=true;
	//	pck.started=true;
	reset();
	appendMessage(BASLANGIC_AYIRACI);
	char tmp[10];
	uint8_t index = 0;
	sprintf(tmp, "%d", targetId);
	while (tmp[index] != '\0')
		appendMessage(tmp[index++]);
	appendMessage(ANA_DATA_AYIRACI);
	sprintf(tmp, "%d", actionCode);
	index = 0;
	while (tmp[index] != '\0')
		appendMessage(tmp[index++]);
	appendMessage(ANA_DATA_AYIRACI);
	if (dataMiktari!=0 && datas == 0x0)
		dataMiktari = 0;
	for (index = 0; index < dataMiktari; index++) {
		for (uint8_t j = 0; datas[index][j] != '\0'; j++) {
			appendMessage(datas[index][j]);
		}
		if (index != dataMiktari - 1)
			appendMessage(ARA_DATA_AYIRACI);
	}

	appendMessage(BITIS_AYIRACI);

}
