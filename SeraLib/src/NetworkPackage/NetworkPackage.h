/*
 * NetworkPackage.h
 *
 *  Created on: 12 Ara 2020
 *      Author: ISIK
 */

#ifndef NETWORKPACKAGE_H_
#define NETWORKPACKAGE_H_

#define BASLANGIC_AYIRACI '<'
#define BITIS_AYIRACI '>'
#define ANA_DATA_AYIRACI ':'
#define ARA_DATA_AYIRACI ';'
#define MAXIMUM_PACKAGE_SIZE 100
// gonderilen data server data : <targetid:ac:d1;d2;d3;,,,>
//alinan client data : <senderid:ac:d1;d2;d3,...>
//orn<1001:12:203.12;203.22>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<Arduino.h>
#include<GeneralSystemConstants.h>
//#include"GeneralSystemConstants.h"
//#ifndef NETWORK_PACKAGE_NO_DATA
//#define NETWORK_PACKAGE_NO_DATA ""
//#endif

class NetworkPackage {
private:
	bool completed;
	bool started;
	char message[MAXIMUM_PACKAGE_SIZE];
	uint8_t messageSize;
bool isServerPackage;
	void parse();
public:
	 static const char *SERVER_END_OF_LINE;
	NetworkPackage();
	static NetworkPackage createServerPackage(uint16_t targetId,uint16_t actionCode, char *datas[],uint8_t dataMiktari);
	void changeMessage(uint16_t targetId,uint16_t actionCode, char *datas[],uint8_t dataMiktari);// daha az memory kullanmak icin hali hazirdakini degistir createServerPackage dan farki yeni bi paket olusturmaz hazirkini degistirir
	void reset();
	uint16_t getActionCode();
	uint16_t getId();// Client package da senderId ServerPackage da targetId yi refer eder
	bool appendMessage(char c);
	bool setMessage(char *c,int len);
	uint8_t getDataMiktari();
	bool isDataCompleted();
	char*getMessage();
	uint8_t getMessageLength();
	uint8_t getDataOf(int kacinciData,char *dt);
	uint8_t getDataLine(char *dt);
	virtual ~NetworkPackage();
};

#endif /* NETWORKPACKAGE_H_ */
